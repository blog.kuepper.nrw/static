#!/bin/bash

cd /posts

for FILE in $(find . ! -name "*.svg" -type f -maxdepth 1); do convert -resize 800 ${FILE} thumbs/${FILE}; done

